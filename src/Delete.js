import React, { Component } from 'react';

export default class Delete extends Component {
    constructor(props) {
        super(props);
        // console.log(this.props);
        this.state = {

        }
    }

    deleteItem = () => {
        this.props.deleteData (this.props.index, this.props.data);
    }
    
  render() {
    return (
        <div className='d-flex justify-content-center mt-1 mb-1'>
            <button className='bg-primary text-white remove' onClick={this.deleteItem}>Delete</button>
        </div>
    )
  }
}
