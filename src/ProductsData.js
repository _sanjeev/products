import React, { Component } from "react";
import PageNotFound from "./PageNotFound";
import Loader from "./Loader";
import "./products.css";
import NoProducts from "./NoProducts";
import { Link } from "react-router-dom";
import Lifting from "./Lifting";
import Update from "./Update";
import Delete from "./Delete";

class ProductsData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productsData: [],
      obj: {},
      updateData: false,
      notFoundtheData: false,
      isLoaded: false,
      showForm : false,
      editProduct : false,
    };
  }
  
  setData = (productData, check) => {
    this.setState({
      productsData: productData,
      notFoundtheData: check,
      isLoaded: true,
    });
  };

  fetchData = async () => {
    try {
      const productResponse = await fetch("https://fakestoreapi.com/products");
      const productData = await productResponse.json();
      this.setData(productData, true);
    } catch (error) {
      this.setData(null, false);
    }
  };

  filter = (event) => {
    const { productsData } = this.state;
    const data = productsData;
    const val = event.target.value;
    if (val === "") {
      this.fetchData();
    } else {
      const res = data.filter((key) => {
        if (val.toUpperCase() === key.title.toUpperCase()) {
          return true;
        }
      });
      this.setState({
        productsData: res,
      });
    }
  };

  
  addData = (product) => {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        let allData = product;
        allData.push(data);
        this.setState ({
          productsData: allData,
        })
      });
  };

  updateItem = (res) => {
    const {productsData} = this.state;
    if (res.id !== undefined) {
      if (res.id >= 0 && res.id < productsData.length) {
        productsData[res.id] = res;
        this.setState ({
          productsData: productsData,
        })
      }
    }
  }

  updItem = (changeData, index) => {
    console.log(changeData);
    let res = this.state.productsData;
    console.log(res[index]);
    const obj = {
      title: changeData.title,
      price: changeData.price,
      image: res[index].image,
    };

    res[index] = obj;

    this.setState({
      productData: res,
    })
  }

  // updData = (id) => {
  //   id = 1;
  //   fetch('https://fakestoreapi.com/products/' + id,{
  //           method:"PUT",
  //           headers: { "Content-Type": "application/json" },
  //           body:JSON.stringify(
  //               {
  //                   title: 'test product',
  //                   price: 13.5,
  //                   description: 'lorem ipsum set',
  //                   image: 'https://i.pravatar.cc',
  //                   category: 'electronic'
  //               }
  //           )
  //       })
  //           .then(res=>res.json())
  //           .then(json=>{
  //             this.updateItem (json);
  //           })
  // }

  editData = () => {
    this.setState ({
      editProduct: !this.state.editProduct,
    })
  }

  deleteData = (index, data) => {
    let {productsData} = this.state;
    const res = productsData.filter((key) => {
      return key.id !== data.id;
    })
    this.setState({
      productsData : res,
    })
  }

  componentDidMount = () => {
    this.fetchData();
    this.addData();
    // this.updData();
  };
  render() {
    const { productsData } = this.state;
    // console.log(productsData);
    return (
      <div>
        {this.state.isLoaded === false ? (
          <Loader />
        ) : (
          <div>
            {this.state.notFoundtheData === true ? (
              <div>
                {productsData === null ? (
                  <NoProducts />
                ) : (
                  <div>
                    <div>
                      <h1 className="text-center">Products</h1>
                      <div className="container-fluid">
                        <div className="row">
                          {productsData.map((product, index) => (
                            <div className="col-sm-6 col-12 col-md-4 col-lg-3">
                              {/* <Link
                                to={{ pathname: `/products/${product.id}` }}
                                key={product.id}
                                className="text-decoration-none"
                              > */}
                                <div key={product.id} onClick={this.editData}>
                                  <div
                                    class="card mb-2 d-flex justify-content-between align-items-center"
                                    style={{ height: "500px" }}
                                  >
                                    <img
                                      src={product.image}
                                      class="w-50 card-img-top pt-3"
                                      alt="..."
                                      height="225px"
                                      width="50%"
                                    />
                                    <div class="card-body d-flex justify-content-between align-items-center flex-column">
                                      <h6 class="card-text">{product.title}</h6>
                                      <h3>${product.price}</h3>
                                    </div>
                                    <Update data = {product} showForm = {this.state.showForm} index={index} changeData={this.updItem}/>
                                    <Delete data = {product} index={index} deleteData = {this.deleteData}/>
                                  </div>
                                </div>
                              {/* </Link> */}
                            </div>
                          ))}
                        </div>
                        <div>
                          <Lifting addProduct = {this.addData} product={productsData}/>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <PageNotFound />
            )}
          </div>
        )}
      </div>
    );
  }
}

export default ProductsData;
