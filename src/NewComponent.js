import React, { Component } from "react";
import { Link } from "react-router-dom";
export default class NewComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      obj: {},
    };
  }
  componentDidMount = () => {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ obj: data });
      });
  };
  render() {
    const { obj } = this.state;
    console.log(obj);
    return (
      <div className="col-sm-6 col-12 col-md-4 col-lg-3">
        <Link
          to={{ pathname: `/products/${obj.id}` }}
          className="text-decoration-none"
        >
          <div>
            <div
              class="card mb-2 d-flex justify-content-between align-items-center"
              style={{ height: "400px" }}
            >
              <img
                src={obj.image}
                class="w-50 card-img-top pt-3"
                alt="..."
                height="225px"
                width="50%"
              />
              <div class="card-body d-flex justify-content-between align-items-center flex-column">
                <h6 class="card-text">{obj.title}</h6>
                <h3>${obj.price}</h3>
              </div>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}
