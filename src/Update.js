import React, { Component } from "react";
import "./button.css";
import Delete from "./Delete";
export default class Update extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      id: "",
      showForm: false,
      title: this.props.data.title,
      price: this.props.data.price,
    };
  }

  updData = () => {
    this.setState({
      showForm: !this.state.showForm,
    });
  };

  chageData = (event) => {
    console.log(this.props);
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  changeData = (event) => {
    event.preventDefault();
    this.props.changeData(this.state, this.props.index, this.props.data);
    this.setState({
      showForm: !this.state.showForm,
    });
  };

  render() {
    return (
      <div className="d-flex justify-content-center">
        {this.state.showForm === false ? (
          <div>
            <button
            className="bg-primary text-white remove"
            onClick={this.updData}
          >
            Update Item
          </button>
          </div>
        ) : (
          <form>
            <div class="form-group mb-1">
              <input
                type="text"
                class="form-control"
                name="title"
                placeholder="Enter Title"
                onChange={this.chageData}
                defaultValue={this.props.data.title}
              />
            </div>
            <div class="form-group mb-1">
              <input
                type="number"
                class="form-control"
                name="price"
                placeholder="Enter price"
                onChange={this.chageData}
                defaultValue={this.props.data.price}
              />
            </div>
            <button
              type="submit"
              class="btn btn-primary"
              onClick={this.changeData}
            >
              Modify Item
            </button>
          </form>
        )}
      </div>
    );
  }
}
