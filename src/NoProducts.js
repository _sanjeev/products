import React, { Component } from 'react';

class NoProducts extends Component {
    render() {
        return (
            <div>
                <h1>No Products</h1>
            </div>
        );
    }
}

export default NoProducts;