import React from "react";
import { Link } from "react-router-dom";
import "./footer.css";

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <footer>
          <div className="content">
            <div className="left box">
              <div className="upper">
                <div className="topic">About us</div>
                <p>
                  Shopify is a cool casual-wear brand. Our customers are our
                  focus and we believe in connecting with them to the fullest.
                  We mirror the fondness of today’s youth and intend to sell a
                  unique product line which conforms to latest trend standards
                  at unbeatable prices. 
                </p>
              </div>
              <div className="lower">
                <div className="topic">Contact us</div>
                <div className="phone">
                  <a href="#">+91 956036**</a>
                </div>
                <div className="email">
                  <a href="#">abc@gmail.com</a>
                </div>
              </div>
            </div>
            <div className="middle box">
              <div className="topic">Our Work</div>
              <div>
                <a href="#">Access & Diagnose Systems</a>
              </div>
              <div>
                <a href="#">Integrate technology</a>
              </div>
              <div>
                <a href="#">Build Community</a>
              </div>
            </div>
            <div className="right box">
              <div className="topic">Subscribe us</div>
            </div>
          </div>
          <div className="bottom">
            <p>
              Copyright © 2022 <a href="#">Shopify</a> All rights reserved
            </p>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
