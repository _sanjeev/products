import React, { Component } from "react";
import Navbar from './Navbar';
import ProductsData from './ProductsData';
import Header from './Header';
import Footer from "./Footer";

class Home extends Component {
  constructor (props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div>
          <Navbar />
          <Header />
          <ProductsData />
          <Footer />
        </div>
      </div>
    );
  }
}

export default Home;
