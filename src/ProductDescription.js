import React, { Component } from "react";
import Loader from "./Loader";
import NoProducts from "./NoProducts";
import PageNotFound from "./PageNotFound";
class ProductDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      isLoading: false,
      notFoundtheData: false,
    };
    console.log(this.props.match.params.id);
  }
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const pdtResponse = await fetch(`https://fakestoreapi.com/products/${id}`);
    const pdtData = await pdtResponse.json();
    this.setState({
      isLoading: true,
      data: pdtData,
      notFoundtheData: true,
    });
  };
  render() {
    const { data } = this.state;
    console.log(data);
    return (
      <div>
        {this.state.isLoading === false ? (
          <Loader />
        ) : (
          <div>
            {this.state.notFoundtheData === true ? (
              <div>
                {data === null ? (
                  <NoProducts />
                ) : (
                  <div>
                    <div className="container-fluid">
                      <div className="row">
                          <div className="col-lg-3 col-md-4 d-flex justify-content-center align-items-center">
                          <img src={data.image} alt="Image" className="mx-5 h-100 w-75" />
                          </div>

                          <div className="col-lg-9 col-md-8 mt-2 d-flex justify-content-center flex-column">
                            <h2><strong>{data.title}</strong></h2>
                            <h4>${data.price}</h4>
                            <div><strong>{data.category}</strong></div>
                            <div><strong>Description:</strong> {data.description}</div>
                            <div><strong>Ratings:</strong> {data.rating.rate}</div>
                            <div><strong>Reviews:</strong> {data.rating.count}</div>
                          </div>
                        </div>
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <PageNotFound />
            )}
          </div>
        )}
      </div>
    );
  }
}

export default ProductDescription;
