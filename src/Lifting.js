import React, { Component } from "react";
import Loader from "./Loader";
import './button.css';
import PageNotFound from "./PageNotFound";
import NoProducts from "./NoProducts";
import { Link } from "react-router-dom";
import NewComponent from "./NewComponent";

export default class Lifting extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  pushItem = () => {
    this.props.addProduct(this.props.product);
  }

  render() {
    const { product } = this.props;
    console.log(product);
    return (
      <div className="d-flex justify-content-center mt-5">
        <button className="bg-primary text-white remove" onClick={this.pushItem} >Add Item</button>
      </div>
    );
  }
}

