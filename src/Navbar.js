import React from "react";
import { div } from "react-router-dom";
import "./navbar.css";

class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="navbar">
        <div class="hamburger">
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className="name">
          <h2>Shopify</h2>
        </div>
        <div class="cart-icon">
          <i class="fa fa-shopping-cart"></i>
        </div>
        <div className="link-div">
          <div className="links">Mens's Clothing</div>
          <div className="links">Women's Clothing</div>
          <div className="links">Electronics</div>
          <div className="links">Jewelery</div>
        </div>
      </div>
    );
  }
}

export default Navbar;