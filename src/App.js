import React, { Component } from 'react';
import {BrowserRouter as Router, Link, Switch, Route} from 'react-router-dom';
import ProductDescription from './ProductDescription';
import Home from './Home';

class App extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/products/:id' component={ProductDescription}/>
      </Switch>
      </Router>
    );
  }
}

export default App;